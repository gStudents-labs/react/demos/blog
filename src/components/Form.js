import React from 'react';

function Form({handleChange, form, postPost}) {
    return (<form>
        <div>
          <input type="text" onChange={handleChange} value={form.author} id="author" placeholder="Author Name" />
        </div>
        <div>
          <input type="text" onChange={handleChange} value={form.title} id="title" placeholder="Title" />
        </div>
        <div>
          <input type="text" onChange={handleChange} value={form.img_url} id="img_url" placeholder="Image URL" />
        </div>
        <div>
          <textarea type="text" onChange={handleChange} value={form.content} id="content" placeholder="Content" />
        </div>
        <button id="addPost" onClick={postPost}>{form.id ? 'Edit Post' : 'Add Post'}</button>
      </form>)
}

export default Form;