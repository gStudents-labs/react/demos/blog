import React from 'react';

function Post({item, upvote, downvote, deletePost}) {
    return (
        <div>
            <img src={item.img_url} alt={item.title} style={{ maxWidth: '50vh' }} />
            <h1>{item.title}</h1>
            <h2>by {item.author}</h2>
            <div><button onClick={() => upvote(item.id)}>&uarr;</button>Votes: {item.votes}<button onClick={() => downvote(item.id)}>&darr;</button></div>
            <div><button onClick={() => deletePost(item.id)}> Delete</button></div>
            <p>{item.content}</p>
        </div>
    )
}

export default Post;