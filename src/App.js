import React, { useReducer, useEffect, useState, useRef } from 'react';
import './App.css';
import Form from './components/Form';
import Post from './components/Post';


const initialState = {
  posts: [],
  url: 'https://dg-collective-api.herokuapp.com/api/posts'
};

function reducer(state, action) {
  switch (action.type) {
    
    case 'create':
      return { ...state, posts: state.posts.concat(action.payload) }
    case 'upvote':
    case 'downvote':
      return {...state, posts: state.posts.map((post) =>
        post.id === action.payload.id
          ? {
              ...action.payload
            }
          : post
      )};
    case 'delete':
      return { ...state, posts: state.posts.filter(post => post.id !== action.payload.id)}
    case 'list':
      return { ...state, posts: action.payload };
    default:
      throw new Error();
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [isLoading, setLoadingState] = useState(false);
  const [form, setForm] = useState({ author: '', title: '', content: '', img_url: '' });

  useEffect(() => {
    getBlogPosts();
  }, [])

  const handleChange = (event) => {
    const newValue = event.target.value;
    const newId = event.target.id;
    setForm({ ...form, [newId]: newValue });
  }

  const getBlogPosts = () => {
    setLoadingState(true);
    const options = {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
    };
    fetch(state.url, options)
      .then(response => response.json())
      .then(data => {
        dispatch({ type: 'list', payload: data }) 
        setLoadingState(false);
      });
  }

  const postPost = (event) => {
    event.preventDefault();
    const newPost = {
      author: form.author,
      content: form.content,
      title: form.title,
      img_url: form.img_url
    }
    const options = {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newPost) // body data type must match "Content-Type" header
    };
    fetch(state.url, options).then((response) => {
      return response.json();
    }).then((response) => {
      dispatch({ type: 'create', payload: response }) // Updating just a single record
      setForm({ author: '', title: '', content: '', img_url: '' }); // resets the state of the local form
    })
  }
  
  const upvote = (id) => {
    const options = {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
    };
    fetch(`${state.url}/votes/increase/${id}`, options).then((response) => {
      return response.json();
    }).then((response) => {
      dispatch({ type: 'upvote', payload: response }) // Updating just a single record
    })
  }

  const downvote = (id) => {
    const options = {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
    };
    fetch(`${state.url}/votes/decrease/${id}`, options).then((response) => {
      return response.json();
    }).then((response) => {
      dispatch({ type: 'downvote', payload: response }) 
    })
  }

  const deletePost = (id) => {
    const options = {
      method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
    };
    fetch(`${state.url}/${id}`, options).then((response) => {
      return response;
    }).then((response) => {
      dispatch({ type: 'delete', payload: {id: id} }) 
    }).catch(error => console.log(error))
  }

  return (
    <div className="App">
      <Form handleChange={handleChange} form={form} postPost={postPost}/>
      {isLoading ? "Loading..." : state.posts.map((item, index) => {
        return (<Post key={index} item={item} upvote={upvote} downvote={downvote} deletePost={deletePost} />)
      })}
    </div>
  );
}

export default App;
